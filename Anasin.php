<?php

class Anasin
{
    private $tokens;//Só tokens
    private $lexemas;//Só lexemas
    private $tabela;//Tokens, lexemas e linha
    private $index;//Para saber qual token está analisando
    private $tokAtual;//Token atual
    private $lexAtual;//Lexema atual
    private $qtdTokens;//Tokens analisados
    private $ts;//Tabela de simbolos
    private $mepa;//Tabela da mepa
    private $qtdVarDecl;//Qtde de variaveis declaradas no programa

    public function __construct($tabelaTokens, $Tabelasimbolos)
    {
        $this->tabela = $tabelaTokens;
        $this->ts = $Tabelasimbolos;
        foreach ($tabelaTokens as $key => $obj) {
            $this->tokens[] = $obj['token'];
            $this->lexemas[] = $obj['lexema'];
        }
        $this->index = 0;
        $this->qtdTokens = count($this->tokens);
        $this->qtdVarDecl = 0;
        $this->mepa = [];
    }

    public function sintatico()
    {
        $this->mepa[] = 'INPP';//MEPA INICIO PROGRAMA
        try {
            //Analisa todos os tokens
            while ($this->nextTok()) {
                //Verifica se o programa começa com uma variavel ou main
                switch ($this->tokAtual) {
                    case Constants::$tabelaTokens['var'];
                    case Constants::$tabelaTokens['main'];
                        $this->decl();
                        break;
                    default:
                        throw new Exception('Era esperado a declaração de uma variável ou do MAIN');
                }
            }
        } catch (Exception $e) {
            $msg = '===========ERRO SINTÁTICO===========' . PHP_EOL;
            $msg .= "Linha do programa: {$this->tabela[$this->index]['linha']}\n";
            $msg .= "Tokens analisados: {$this->index}\n";
            $msg .= "Erro: {$e->getMessage()} \n";
            $msg .= 'Token recebido: ' . $this->tokAtual;
            //Se houver erro sintático, gera uma excessão
            throw new Exception($msg);
        }

        $this->mepa[] = "DMEM {$this->qtdVarDecl}";//MEPA DESALOCA VARIAVEIS
        $this->mepa[] = 'PARA';//MEPA TERMINO PROGRAMA
        echo PHP_EOL . 'Analise sintática realizada com sucesso!' . PHP_EOL;
    }

    //Declaração do main ou variáveis
    private function decl()
    {
        $this->debug(__METHOD__);
        switch ($this->tokAtual) {
            case Constants::$tabelaTokens['var'];
                $this->declVar();
                break;
            case Constants::$tabelaTokens['main'];
                $this->declMain();
                break;

        }
    }

    // Declaração de variáveis
    private function declVar()
    {
        $this->debug(__METHOD__);
        //Vai para o proximo token
        $this->nextTok();
        $this->especTipo();//Analisa se é um tipo válido
        $tipo = $this->lexemas[$this->index - 1]; // pega o tipo da variavel declarada
        $lexAtual = $this->index; //salva o index para buscar o lexema
        $this->nextTok();
        if ($this->tokAtual != Constants::$tabelaTokens['ID']) {//Se não for um identificador
            throw new Exception('Era esperado um identificador na declaração de variável');
        }

        $this->nextTok();
        if ($this->tokAtual != Constants::$tabelaTokens[';']) {//Se não terminar com ;
            throw new Exception('Falta ponto e virgula após a declaração.');
        }
        $id = $this->lexemas[$lexAtual]; // busca o lexema da variavel declarada
        $this->ts->ts_insere($id, $tipo, "var"); // insere na tabela de simbolo

        //MEPA declara variaveis
        $this->mepa[] = 'AMEM 1';
        $this->qtdVarDecl++;
    }

    //Verifica se o tipo declarado é valido
    private function especTipo()
    {
        $this->debug(__METHOD__);
        //Se não for um tipo aceito, gera excessão
        if (
            $this->tokAtual != Constants::$tabelaTokens['int'] &&
            $this->tokAtual != Constants::$tabelaTokens['float'] &&
            $this->tokAtual != Constants::$tabelaTokens['char'] &&
            $this->tokAtual != Constants::$tabelaTokens['real'] &&
            $this->tokAtual != Constants::$tabelaTokens['bool']
        ) {
            throw new Exception('Esperado tipo após declaração de variável.');
        }
    }

    // Declaração do main
    private function declMain()
    {
        $this->debug(__METHOD__);
        //Vai para o proximo token
        $this->nextTok();
        if ($this->tokAtual != Constants::$tabelaTokens['(']) {//Verifica se abriu o parenteses
            throw new Exception('Esperado abertura do parenteses após o MAIN');
        }
        $this->nextTok();
        if ($this->tokAtual != Constants::$tabelaTokens[')']) {//Verifica se fechou o parenteses
            throw new Exception('Parenteses do MAIN não foi fechado');
        }
        $tipo = 'void';
        $this->ts->ts_insere('main', $tipo, Constants::$tabelaTokens[$tipo]); // insere na tabela de simbolo
        $this->bloco();
        if ($this->tokAtual != Constants::$tabelaTokens['end']) {//Verifica se terminou o main
            throw new Exception('Declaração do MAIN não finalizada.');
        }
    }

    //Bloco do main
    private function bloco()
    {
        $this->debug(__METHOD__);
        $this->nextTok();
        $this->listaCom();
    }

    //Lista de comandos
    private function listaCom()
    {
        do {
            $this->debug(__METHOD__);
            $loop = false;//Permite a declaração de várias variáveis sem chamada recursiva
            switch ($this->tokAtual) {
                case Constants::$tabelaTokens['var'];//Declaracao de variavel
                    $this->declVar();
                    $this->nextTok();
                    $loop = true;
                    break;
                case Constants::$tabelaTokens['ID'];//Atribuição
                    $this->comAtrib();
                    $this->nextTok();
                    $loop = true;
                    break;

                case Constants::$tabelaTokens['print'];
                case Constants::$tabelaTokens['printLn'];
                    $loop = true;
                case Constants::$tabelaTokens['scan'];
                case Constants::$tabelaTokens['scanLn'];
                    //Comando com um parâmetro
                    $this->comandoUmParametro($loop);
                    $this->nextTok();
                    $loop = true;
                    break;

                case Constants::$tabelaTokens['if'];//Operação Se/Senao
                    $this->selecao();
                    $this->nextTok();
                    $loop = true;
                    break;
                case Constants::$tabelaTokens['while'];//Repetição
                case Constants::$tabelaTokens['do'];//Repetição
                    $this->repeticao();
                    $this->nextTok();
                    $loop = true;
                    break;
                case Constants::$tabelaTokens['end']:
                    break;
                default:
//                    throw new Exception('Token não esperado');
            }
        } while ($loop);
    }

    //Atribuição de variável
    private function comAtrib()
    {
        //Serve para saber onde armazenar após a expressão
        $armazena = $this->lexAtual;

        $this->debug(__METHOD__);
        $this->nextTok();
        if ($this->tokAtual != Constants::$tabelaTokens['=']) {//Se achou um identificador e não achou atribuição, volta pois pode ser outra operação
            throw new Exception('Esperado expressão para atribuir ao identificador');
        }
        //Se achou continua a anlise
        $this->nextTok();
        $this->exp();
        $this->nextTok();
        if ($this->tokAtual != Constants::$tabelaTokens[';']) {
            throw new Exception('Falta ponto e vírgula após atribuição.');
        }

        $this->mepa[] = "ARMZ {$armazena}";
    }

    /**
     * @param bool $outros -> Serve para indicar que aceita outras coisas além de um identificador
     * @throws Exception
     */
    private function comandoUmParametro($outros = false)
    {
        $this->debug(__METHOD__);
        $this->nextTok();
        if ($this->tokAtual != Constants::$tabelaTokens['(']) {
            throw new Exception('Esperado parenteses após comando.');
        }
        $this->nextTok();
        if (
            $outros &&
            $this->tokAtual != Constants::$tabelaTokens['"'] &&
            $this->tokAtual != Constants::$tabelaTokens['\''] &&
            $this->tokAtual != Constants::$tabelaTokens['INTEIRO'] &&
            $this->tokAtual != Constants::$tabelaTokens['FLUTUANTE'] &&
            $this->tokAtual != Constants::$tabelaTokens['ID']
        ) {
            throw new Exception('Esperado identificador como parâmetro do comando.');
        } elseif (!$outros && $this->tokAtual != Constants::$tabelaTokens['ID']) {
            throw new Exception('Esperado identificador como parâmetro do comando.');
        }
        $this->nextTok();
        if ($this->tokAtual != Constants::$tabelaTokens[')']) {
            throw new Exception('Comando não fechou parenteses.');
        }
        $this->nextTok();
        if ($this->tokAtual != Constants::$tabelaTokens[';']) {
            throw new Exception('Falta ponto e vírgula após comando.');
        }
    }

    // IF/ELSEIF
    private function selecao()
    {
        $this->debug(__METHOD__);
        $this->nextTok();
        if ($this->tokAtual != Constants::$tabelaTokens['(']) {
            throw new Exception('IF sem abrir parenteses');
        }
        $this->nextTok();
        $this->exp();
        $this->nextTok();
        if ($this->tokAtual != Constants::$tabelaTokens[')']) {
            throw new Exception('IF sem fechar parenteses');
        }
        $this->nextTok();
        if ($this->tokAtual != Constants::$tabelaTokens['then']) {
            throw new Exception('Esperado THEN após expressão do IF');
        }
        $this->bloco();

        if ($this->tokAtual == Constants::$tabelaTokens['endif']) {//IF sem ELSE
            return;
        } elseif ($this->tokAtual != Constants::$tabelaTokens['else']) {
            throw new Exception('Esperado ENDIF ou ELSE após o IF');
        }

        //IF COM ELSE
        $this->bloco();

        if ($this->tokAtual != Constants::$tabelaTokens['endif']) {//FIM DO ELSEIF
            throw new Exception('Esperado ENDIF');
        }
    }

    // Laços de repetição
    private function repeticao()
    {
        if ($this->tokAtual == Constants::$tabelaTokens['while']) {
            $this->repeticaoWhile();
        } elseif ($this->tokAtual == Constants::$tabelaTokens['do']) {
            $this->repeticaoDo();
        }
    }

    private function repeticaoWhile()
    {
        $this->debug(__METHOD__);
        $this->nextTok();
        $this->exp();
        $this->nextTok();
        if ($this->tokAtual != Constants::$tabelaTokens['do']) {
            throw new Exception('Esperado DO após WHILE');
        }
        $this->bloco();

        if ($this->tokAtual != Constants::$tabelaTokens['loop']) {
            throw new Exception('Esperado LOOP no término do WHILE');
        }
        $this->nextTok();
        if ($this->tokAtual != Constants::$tabelaTokens[';']) {
            throw new Exception('Falta ponto e vírgula após LOOP.');
        }


    }

    private function repeticaoDo()
    {
        $this->debug(__METHOD__);
        $this->bloco();

        if ($this->tokAtual != Constants::$tabelaTokens['while']) {
            throw new Exception('Esperado WHILE após bloco DO   ');
        }
        $this->exp();
        if ($this->tokAtual != Constants::$tabelaTokens[';']) {
            throw new Exception('Falta ponto e vírgula após expressão DO WHILE.');
        }

    }

    // Verifica se encontrou um operador relacional
    private function opRelac()
    {
        $this->debug(__METHOD__);
        $this->nextTok();

        //Verifica se é um operador relacional
        if (
            $this->tokAtual == Constants::$tabelaTokens['<='] ||
            $this->tokAtual == Constants::$tabelaTokens['<'] ||
            $this->tokAtual == Constants::$tabelaTokens['>'] ||
            $this->tokAtual == Constants::$tabelaTokens['>='] ||
            $this->tokAtual == Constants::$tabelaTokens['=='] ||
            $this->tokAtual == Constants::$tabelaTokens['!=']
        ) {
            $this->mepa[] = Constants::$tabelaMepa[$this->tokAtual];
            return true;
        }

        //Se não encontrou, volta para o token anterior
        $this->prevTok();
        return false;

    }

    private function opSoma()
    {
        $this->debug(__METHOD__);
        $this->nextTok();

        //Verifica se é um operador soma
        if (
            $this->tokAtual == Constants::$tabelaTokens['+'] ||
            $this->tokAtual == Constants::$tabelaTokens['-'] ||
            $this->tokAtual == Constants::$tabelaTokens['||']
        ) {
            $this->mepa[] = Constants::$tabelaMepa[$this->tokAtual];
            return true;
        }

        //Se não encontrou, volta para o token anterior
        $this->prevTok();
        return false;

    }

    private function opMult()
    {
        $this->debug(__METHOD__);
        $this->nextTok();

        //Verifica se é um operador soma
        if (
            $this->tokAtual == Constants::$tabelaTokens['*'] ||
            $this->tokAtual == Constants::$tabelaTokens['/'] ||
            $this->tokAtual == Constants::$tabelaTokens['%'] ||
            $this->tokAtual == Constants::$tabelaTokens['&&']
        ) {
            $this->mepa[] = Constants::$tabelaMepa[$this->tokAtual];
            return true;
        }

        //Se não encontrou, volta para o token anterior
        $this->prevTok();
        return false;

    }

    //Expressões matemáticas
    private function exp()
    {
        $this->debug(__METHOD__);
        //exp -> exp-soma op-relac exp-soma | exp-soma
        $this->expSoma();
        if ($this->opRelac()) {
            $this->nextTok();
            $this->expSoma();
        }

    }

    private function expSoma()
    {
        $this->debug(__METHOD__);
        $this->expMult();
        if ($this->opSoma()) {
            $this->nextTok();
            $this->expSoma();
        }
    }

    private function expMult()
    {
        $this->debug(__METHOD__);
        if (!$this->expSimples()) {//Se não for expressão simples, é uma expressão multipla
            $this->nextTok();
            $this->expMult();
            if (!$this->opMult()) {
                throw new Exception('Esperado operador * / % ou &&');
            }
            $this->nextTok();
            if (!$this->expSimples()) {
                throw new Exception('Esperado uma expressão simples.');
            }
        } else {
            $tam = count($this->mepa);
            $tmp = $this->mepa[$tam-1];
            $tmp2 = $this->mepa[$tam-2];
            //Só pode inverter se a operação anterior for soma, mult, subt, ...
            //POS FIXO
            if(
                $tmp2 == Constants::$tabelaMepa[Constants::$tabelaTokens['+']] ||
                $tmp2 == Constants::$tabelaMepa[Constants::$tabelaTokens['-']] ||
                $tmp2 == Constants::$tabelaMepa[Constants::$tabelaTokens['*']] ||
                $tmp2 == Constants::$tabelaMepa[Constants::$tabelaTokens['/']] ||
                $tmp2 == Constants::$tabelaMepa[Constants::$tabelaTokens['%']] ||
                $tmp2 == Constants::$tabelaMepa[Constants::$tabelaTokens['&&']] ||
                $tmp2 == Constants::$tabelaMepa[Constants::$tabelaTokens['||']] ||
                $tmp2 == Constants::$tabelaMepa[Constants::$tabelaTokens['>']] ||
                $tmp2 == Constants::$tabelaMepa[Constants::$tabelaTokens['<']] ||
                $tmp2 == Constants::$tabelaMepa[Constants::$tabelaTokens['>=']] ||
                $tmp2 == Constants::$tabelaMepa[Constants::$tabelaTokens['<=']]
            ) {
                $this->mepa[$tam-1] = $tmp2;
                $this->mepa[$tam-2] = $tmp;
            }
        }

    }

    private function expSimples()
    {
        $this->debug(__METHOD__);
        if ($this->tokAtual == Constants::$tabelaTokens['(']) {
            $this->exp();
            $this->nextTok();
            if ($this->tokAtual != Constants::$tabelaTokens[')']) {
                throw new Exception('Expressão não fechou parenteses');
            }
        } elseif (
            $this->tokAtual != Constants::$tabelaTokens['ID'] &&
            $this->tokAtual != Constants::$tabelaTokens['INTEIRO'] &&
            $this->tokAtual != Constants::$tabelaTokens['\''] &&
            $this->tokAtual != Constants::$tabelaTokens['"'] &&
            $this->tokAtual != Constants::$tabelaTokens['FLUTUANTE']
        ) {
            //verifica se é uma expressão simples
            return false;
        }

        if (
            $this->tokAtual == Constants::$tabelaTokens['INTEIRO'] ||
            $this->tokAtual == Constants::$tabelaTokens['\''] ||
            $this->tokAtual == Constants::$tabelaTokens['"'] ||
            $this->tokAtual == Constants::$tabelaTokens['FLUTUANTE']) {
            $this->mepa[] = "CRCT {$this->lexAtual}";
        } else if($this->tokAtual == Constants::$tabelaTokens['ID']) {
            $this->mepa[] = "CRCV {$this->lexAtual}";
        }

        return true;//Se nao for uma expressão simples, então é uma expressão mult
    }

    private function nextTok()
    {
        $this->debug(__METHOD__);
        //Se for o fim do programa, retorna falso
        if ($this->index >= $this->qtdTokens) {
            $this->tokAtual = false;
            return false;
        }

        //Pega o próximo token
        $this->tokAtual = $this->tokens[$this->index];
        $this->lexAtual = $this->lexemas[$this->index++];

        return true;
    }

    private function prevTok()
    {
        $this->debug(__METHOD__);
        //Se for o primeiro, então não consegue voltar
        if (!--$this->index) {
            $this->tokAtual = false;
            return false;
        }

        //Pega o token anterior
        $this->tokAtual = $this->tokens[$this->index-1];
        $this->lexAtual = $this->lexemas[$this->index-1];

        return true;
    }

    private function debug($declaracao)
    {
        if (DEBUG) {
            echo "Analisando a declaração {$declaracao} - Index: {$this->index} - Token atual: {$this->tokAtual} - Lexema atual: {$this->lexAtual}\n";
        }
    }

    public function imprimeMepa()
    {
        echo '=============MEPA================' . PHP_EOL;
        foreach ($this->mepa as $mepa) {
            echo $mepa . PHP_EOL;
        }
    }
}