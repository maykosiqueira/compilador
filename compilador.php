<?php
/**
 * Created by PhpStorm.
 * User: maykon
 * Date: 14/06/18
 * Time: 22:25
 */

require_once('Constants.php');
require_once('Analex.php');
require_once('Anasin.php');
require_once('Tabelasimbolos.php');
define('DEBUG', false);
define('ARQUIVO', 'Arquivos/1.d');

try {
    //Realiza a analise lexica
    $analex = new Analex(ARQUIVO);
    $analex->lexico();
    $analex->imprimeAnalex();
    $tabelaTokens = $analex->getTokens();

    //Realiza a análise sintatica
    $ts = new Tabelasimbolos();
    $anasin = new Anasin($tabelaTokens, $ts);
    $anasin->sintatico();

    //Monta a tabela de simbolos
    $ts->ts_imprimi();
    $anasin->imprimeMepa();

} catch (Exception $e) {
    echo $e->getMessage() . "\n";
}
