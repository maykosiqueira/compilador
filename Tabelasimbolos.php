<?php

class TabelaSimbolos
{
    public $ts = [];

    public function __construct(){}

    public function ts_insere($nome, $tipo, $categoria){
        try{
            if(!$this->ts_consulta($nome)){
                $this->ts[] = ["nome" => $nome,
                    "tipo" => $tipo,
                    "categoria" => $categoria];
            }else{
                throw new Exception("A variavel {$nome} já foi declarada.");
            }       
        }catch (Exception $e){
            throw new Exception($e->getMessage());
        }
    }

    public function ts_consulta($nome){
        foreach ($this->ts as $key => $simb) {
            if($nome == $simb['nome']){
                return true;
            }
        }
        return false;
    }

    public function ts_imprimi(){
        echo "\n";
        echo "+============TABELA SIMBOLOS===========+\n";
        $mask = "|%-5s|%-10s|%-10s|%-10s| \n";
        printf($mask, 'Id', 'Nome', 'Tipo', 'Categoria');
        echo "+=====|==========|==========|==========+\n";
        foreach ($this->ts as $key => $simb) {
            printf($mask, $key, $simb['nome'], $simb['tipo'], $simb['categoria']);
        }
        echo "+======================================+\n";
    }
}