<?php
/**
 * Class Analex
 * TODO: ver como detectar funçoes, como minha_funcao ()
 */


class Analex
{
    private $erros = [];
    private $log = [];
    private $programa;
    private $tamanhoPrograma;
    private $index;
    private $palavraTmp;
    private $charAtual;
    private $linha;

    //Lista de tokens e lexemas encontrados
    private $tokens = [];

    public function __construct($arquivo)
    {
        $this->debug(__METHOD__);

        // Carrega o programa que será compilado
        if (file_exists($arquivo)) {
            $this->programa = file($arquivo, FILE_SKIP_EMPTY_LINES);
        } else {
            $this->programa = $arquivo;
        }

        //Inicia no caracter 0
        $this->index = 0;

        //Linha do arquivo
        $this->linha = 1;
    }

    public function lexico()
    {
        $this->debug(__METHOD__);

        //Deixa o código em uma única string
        $this->programa = implode('', $this->programa);

        // Pega o tamanho do programa
        $this->tamanhoPrograma = strlen($this->programa);

        try {
            //Analisa caracter por caracter
            while ($this->nextChar()) {
                $this->debug('LOOP');
                if (ctype_alpha($this->charAtual)) {
                    //Se for um caracter de texto
                    $this->tokens[] = $this->estado2();
                } elseif (ctype_digit($this->charAtual)) {
                    //Se for um número
                    $this->tokens[] = $this->estado3();
                } elseif ($this->charAtual == '"') {
                    //Se abrir aspas duplas
                    $this->tokens[] = $this->estado8();
                } elseif ($this->charAtual == "'") {
                    //Se abrir aspas simples
                    $this->tokens[] = $this->estado6();
                } elseif ($this->charAtual == '/') {
                    //Se for um indicio de comentário
                    //ou uma divisao ou um /=
                    $this->estado9();
                } elseif (
                    $this->charAtual == '=' ||
                    $this->charAtual == '!' ||
                    $this->charAtual == '<' ||
                    $this->charAtual == '>' ||
                    $this->charAtual == '*' ||
                    $this->charAtual == '/' ||
                    $this->charAtual == '%'
                ) {
                    //Simbolos especiais
                    $this->tokens[] = $this->estado13();
                } elseif ($this->charAtual == '-') {
                    //Simbolos especiais
                    $this->tokens[] = $this->estado14();
                } elseif ($this->charAtual == '+') {
                    //Simbolos especiais
                    $this->tokens[] = $this->estado15();
                } elseif ($this->charAtual == '&') {
                    //Simbolos especiais
                    $this->tokens[] = $this->estado16();
                } elseif ($this->charAtual == '|') {
                    //Simbolos especiais
                    $this->tokens[] = $this->estado17();
                } elseif ($this->charAtual == '$') {
                    //Identificadores
                    $this->tokens[] = $this->estado18();
                } elseif (
                    $this->charAtual == '?' ||
                    $this->charAtual == ':' ||
                    $this->charAtual == '.' ||
                    $this->charAtual == ',' ||
                    $this->charAtual == ';' ||
                    $this->charAtual == '(' ||
                    $this->charAtual == ')' ||
                    $this->charAtual == '[' ||
                    $this->charAtual == ']' ||
                    $this->charAtual == '_' ||
                    $this->charAtual == '{' ||
                    $this->charAtual == '}'
                ) {
                    //Simbolos especiais
                    $this->tokens[] = [
                        'token' => Constants::$tabelaTokens[$this->charAtual],
                        'lexema' => $this->charAtual,
                        'linha' => $this->linha
                    ];
                } elseif ($this->charAtual == "\n") {
                    $this->linha++;
                } elseif (
                    $this->charAtual != "\t" &&
                    $this->charAtual != "\r" &&
                    $this->charAtual != ' '
                ) {
                    throw new Exception("O lexema {$this->charAtual} não é reconhecido. - Estado 0");
                }
                $this->finalizaEstado();
            }
            echo PHP_EOL . 'Analise léxica realizada com sucesso!' . PHP_EOL;
        } catch (Exception $e) {
            $msg = '===========ERRO LEXICO===========' . PHP_EOL;
            $msg .= "Linha do programa: {$this->linha}\n";
            $msg .= "Caracteres analisados: {$this->index}\n";
            $msg .= "Erro: {$e->getMessage()} \n";
            $msg .= 'Programa analisado: ' . PHP_EOL;
            $msg .= substr($this->programa, 0, $this->index) . "\n";
            //Se houver erro léxico, gera uma excessão
            throw new Exception($msg);
        }
    }

    /**
     * Pega o próximo caracter
     * @return bool
     */
    private function nextChar()
    {
        $this->debug(__METHOD__);

        //Se for o fim do programa, retorna falso
        if ($this->index >= $this->tamanhoPrograma) {
            $this->charAtual = false;
            return false;
        }

        //Pega o próximo caracter
        $this->charAtual = $this->programa[$this->index++];

        return true;
    }

    /**
     * Pega o caracter anterior
     * @return bool
     */
    private function prevChar()
    {
        $this->debug(__METHOD__);

        //Se for o primeiro, então não consegue avançar
        if (!$this->index) {
            $this->charAtual = false;
            return false;
        }

        //Pega o caracter anterior
        $this->charAtual = $this->programa[--$this->index];

        return true;
    }

    /**
     * ESTADOS DO AUTOMATO
     */
    private function estado2()
    {
        $this->debug(__METHOD__);

        do {
            //Se não for letra
            if (!ctype_alpha($this->charAtual)) {
                break;
            }

            //Adiciona o caracter na palavra temporária
            $this->palavraTmp .= $this->charAtual;
        } while ($this->nextChar());

        //Volta um caracter para ser analisado posteriormente
        if ($this->charAtual) {
            $this->prevChar();
        }

        if (array_key_exists($this->palavraTmp, Constants::$tabelaTokens)) {
            return [
                'token' => Constants::$tabelaTokens[$this->palavraTmp],
                'lexema' => $this->palavraTmp,
                'linha' => $this->linha
            ];
        } else {
            throw new Exception("Lexema {$this->palavraTmp} não reconhecido. - Estado 2");
        }
    }

    //Numeros
    private function estado3($ignorarPonto = false)
    {
        $this->debug(__METHOD__);

        //O ignorarPonto indica que já foi encontrado um ponto, então deve ignorar o segundo se encontrar
        do {
            if ($this->charAtual == '.' && !$ignorarPonto) {
                //Se encontrar ponto, indica que era para ser um número real
                $this->palavraTmp .= $this->charAtual;
                return $this->estado4();
            } elseif (!ctype_digit($this->charAtual)) {
                //Se vier algo diferente, então acabou o número inteiro
                $this->prevChar();
                return [
                    'token' => $ignorarPonto ? Constants::$tabelaTokens['FLUTUANTE'] : Constants::$tabelaTokens['INTEIRO'],
                    'lexema' => $this->palavraTmp,
                    'linha' => $this->linha
                ];
            }

            $this->palavraTmp .= $this->charAtual;
        } while ($this->nextChar());
    }

    //Ponto do número real
    private function estado4()
    {
        $this->debug(__METHOD__);

        if (!$this->nextChar()) {
            throw new Exception('Fim do programa inesperado. - Estado 4');
        } elseif (!ctype_digit($this->charAtual)) {
            throw new Exception('Era esperado número após o ponto (número real). - Estado 4');
        } else {
            //O estado 5 e o mesmo do 3, com excessão do ponto
            return $this->estado3(true);
        }
    }

    private function estado6()
    {
        $this->debug(__METHOD__);

        //O Estado 7 está sendo feito dentro deste estado
        if (!$this->nextChar()) {
            throw new Exception('Fim do programa inesperado. - Estado 6');
        } elseif ($this->charAtual != "'") {
            $this->palavraTmp = $this->charAtual;
            if (!$this->nextChar()) {
                throw new Exception('Fim do programa inesperado. - Estado 6');
            } elseif ($this->charAtual != "'") {
                throw new Exception('Caracter deve ter tamanho 1. - Estado 6');
            } else {
                return [
                    'token' => Constants::$tabelaTokens["'"],
                    'lexema' => $this->palavraTmp,
                    'linha' => $this->linha
                ];
            }
        } else {
            return [
                'token' => Constants::$tabelaTokens["'"],
                'lexema' => '',
                'linha' => $this->linha
            ];
        }

    }

    private function estado8()
    {
        $this->debug(__METHOD__);

        while ($this->nextChar()) {
            //Se achar o fim da string
            if ($this->charAtual == '"') {
                return [
                    'token' => Constants::$tabelaTokens['"'],
                    'lexema' => $this->palavraTmp,
                    'linha' => $this->linha
                ];
            }

            //Adiciona o caracter na palavra temporária
            $this->palavraTmp .= $this->charAtual;
        }

        throw new Exception('Palavra entre aspas duplas não finalizada. - Estado 8');
    }

    private function estado9()
    {
        $this->debug(__METHOD__);

        //Inicio de comentário
        if (!$this->nextChar()) {
            throw new Exception('Fim do programa inesperado. - Estado 8');
        } elseif ($this->charAtual == '*') {
            return $this->estado10();
        } elseif ($this->charAtual == '/') {
            return $this->estado12();
        } elseif ($this->charAtual == '=') {
            // /=
            return [
                'token' => Constants::$tabelaTokens['/='],
                'lexema' => '/=',
                'linha' => $this->linha
            ];
        } else {
            //Se não for comentário então é uma divisao
            $this->prevChar();
            return [
                'token' => Constants::$tabelaTokens[$this->charAtual],
                'lexema' => $this->charAtual,
                'linha' => $this->linha
            ];
        }
    }

    private function estado10()
    {
        $this->debug(__METHOD__);

        //Estado 11 está sendo feito dentro deste estado
        //Comentário multilinha
        while ($this->nextChar()) {
            if ($this->charAtual == '*') {
                if (!$this->nextChar()) {
                    throw new Exception('Fim do programa inesperado. - Estado 10');
                }
                if ($this->charAtual == '/') {
                    //Se achou uma */ então é fim de comentário
                    return;
                } else {
                    //Se ele encontrou um * mas sem barra, deve voltar o caracter
                    $this->prevChar();
                }
            } elseif ($this->charAtual == PHP_EOL) {
                $this->linha++;
            }
        }
        throw new Exception('Fim do programa inesperado. - Estado 10');
    }

    private function estado12()
    {
        $this->debug(__METHOD__);

        //Comentario de linha unica
        while ($this->nextChar()) {
            if ($this->charAtual == "\n") {
                $this->linha++;
                return;
            }
        }
    }

    private function estado13()
    {
        $this->debug(__METHOD__);

        $this->palavraTmp = $this->charAtual;
        if (!$this->nextChar()) {
            return [
                'token' => Constants::$tabelaTokens[$this->palavraTmp],
                'lexema' => $this->palavraTmp,
                'linha' => $this->linha
            ];
        } else {
            if ($this->charAtual != '=') {
                $this->prevChar();
                return [
                    'token' => Constants::$tabelaTokens[$this->palavraTmp],
                    'lexema' => $this->palavraTmp,
                    'linha' => $this->linha
                ];
            } else {
                $this->palavraTmp .= $this->charAtual;
                return [
                    'token' => Constants::$tabelaTokens[$this->palavraTmp],
                    'lexema' => $this->palavraTmp,
                    'linha' => $this->linha
                ];
            }
        }

    }

    private function estado14()
    {
        $this->debug(__METHOD__);

        $this->palavraTmp = $this->charAtual;
        if (!$this->nextChar()) {
            return [
                'token' => Constants::$tabelaTokens[$this->palavraTmp],
                'lexema' => $this->palavraTmp,
                'linha' => $this->linha
            ];
        } else {
            if ($this->charAtual != '>' && $this->charAtual != '=') {
                $this->prevChar();
                return [
                    'token' => Constants::$tabelaTokens[$this->palavraTmp],
                    'lexema' => $this->palavraTmp,
                    'linha' => $this->linha
                ];
            } else {
                $this->palavraTmp .= $this->charAtual;
                return [
                    'token' => Constants::$tabelaTokens[$this->palavraTmp],
                    'lexema' => $this->palavraTmp,
                    'linha' => $this->linha
                ];
            }
        }
    }

    private function estado15()
    {
        $this->debug(__METHOD__);

        $this->palavraTmp = $this->charAtual;
        if (!$this->nextChar()) {
            return [
                'token' => Constants::$tabelaTokens[$this->palavraTmp],
                'lexema' => $this->palavraTmp,
                'linha' => $this->linha
            ];
        } else {
            if ($this->charAtual != '+' && $this->charAtual != '=') {
                $this->prevChar();
                return [
                    'token' => Constants::$tabelaTokens[$this->palavraTmp],
                    'lexema' => $this->palavraTmp,
                    'linha' => $this->linha
                ];
            } else {
                $this->palavraTmp .= $this->charAtual;
                return [
                    'token' => Constants::$tabelaTokens[$this->palavraTmp],
                    'lexema' => $this->palavraTmp,
                    'linha' => $this->linha
                ];
            }
        }
    }

    private function estado16()
    {
        $this->debug(__METHOD__);

        $this->palavraTmp = $this->charAtual;
        if (!$this->nextChar()) {
            return [
                'token' => Constants::$tabelaTokens[$this->palavraTmp],
                'lexema' => $this->palavraTmp,
                'linha' => $this->linha
            ];
        } else {
            if ($this->charAtual != '&') {
                $this->prevChar();
                return [
                    'token' => Constants::$tabelaTokens[$this->palavraTmp],
                    'lexema' => $this->palavraTmp,
                    'linha' => $this->linha
                ];
            } else {
                $this->palavraTmp .= $this->charAtual;
                return [
                    'token' => Constants::$tabelaTokens[$this->palavraTmp],
                    'lexema' => $this->palavraTmp,
                    'linha' => $this->linha
                ];
            }
        }
    }

    private function estado17()
    {
        $this->debug(__METHOD__);

        $this->palavraTmp = $this->charAtual;
        if (!$this->nextChar()) {
            throw new Exception('Fim do programa inesperado. - Estado 17');
        } else {
            if ($this->charAtual != '|') {
                $this->palavraTmp .= $this->charAtual;
                $this->prevChar();
                throw new Exception("Lexema {$this->palavraTmp} não reconhecido. - Estado 17");
            } else {
                $this->palavraTmp .= $this->charAtual;
                return [
                    'token' => Constants::$tabelaTokens[$this->palavraTmp],
                    'lexema' => $this->palavraTmp,
                    'linha' => $this->linha
                ];
            }
        }
    }

    private function estado18()
    {
        $this->debug(__METHOD__);

        $this->palavraTmp = $this->charAtual;
        while ($this->nextChar()) {
            if (
                !ctype_alpha($this->charAtual) && //Se for não letra
                !ctype_digit($this->charAtual) && //Se for não numero
                $this->charAtual != '_' // Se não for underscore
            ) {
                if ($this->palavraTmp == '$') {
                    throw new Exception('Identificador sem nome. - Estado 18');
                }
                $this->prevChar();
                return [
                    'token' => Constants::$tabelaTokens['ID'],
                    'lexema' => $this->palavraTmp,
                    'linha' => $this->linha
                ];
            }

            $this->palavraTmp .= $this->charAtual;
        }

        throw new Exception('Fim do programa inesperado. - Estado 18');
    }

    private function finalizaEstado()
    {
        $this->debug(__METHOD__);
        //Limpa a palavra temporária
        $this->palavraTmp = '';
    }

    private function debug($numEstado)
    {
        if (DEBUG) {
            echo "Entrando no {$numEstado} - Index: {$this->index} - Char Atual: {$this->charAtual}\n";
        }
    }

    public function getTokens()
    {
        if (count($this->tokens)) {
            return $this->tokens;
        }

        return false;
    }
    public function imprimeAnalex()
    {

        echo "Programa analisado:\n{$this->programa}\n";
        echo "===============TABELA SIMBOLOS===============\n";
        $mask = "%20s |%-30s \n";
        printf($mask, '===Token===', '===Lexema===');
        foreach ($this->tokens as $key => $token) {
            printf($mask, $token['token'], $token['lexema']);
        }
    }
}